Dado('que acesso ao site Automacao Practice') do
    visit "/index.php"
    screenshot
end

Quando('acesso a pagina de Login com os dados {string} e {string}') do |login, passw|
    find("#header > div.nav > div > div > nav > div.header_user_info > a").click 
    find("#email").set login
    find("#passwd").set passw
    find("#SubmitLogin > span > i").click
    screenshot
    
end

Então('visualizo {string} na pagina logada') do |mensagem|
    expect(page).to have_content mensagem
    sleep 2
end