Dado('que acesso ao site Automacao Practice para cadastro') do
    visit "?controller=authentication&back=my-account"
    find("#email_create").set Faker::Internet.email
    expect(page).to have_content "CREATE AN ACCOUNT"
    find("#SubmitCreate > span > i").click
    screenshot
end

Quando('preencho os campos do cadastro') do
    sleep 5
    page.execute_script("$('#id_gender1').click();")
    #page.execute_script("$('#uniform-id_gender1 > span').click();")
    find("#customer_firstname").set Faker::Name.first_name
    find("#customer_lastname").set Faker::Name.last_name
    find("#passwd").set "12345678"
    screenshot
    find("#days > option:nth-child(25)").click
    find("#months > option:nth-child(8)").click
    find("#years > option:nth-child(28)").click
    screenshot
    find("#firstname").set Faker::Name.first_name
    find("#lastname").set Faker::Name.last_name
    find("#address1").set Faker::Address.street_name
    find("#city").set "São Paulo"
    find("#id_state > option:nth-child(6)").click
    find("#postcode").set Faker::Address.zip_code
    find("#phone_mobile").set Faker::PhoneNumber.cell_phone_in_e164
    find("#alias").set Faker::Address.street_name
    screenshot
    find("#submitAccount > span").click
    sleep 2
end

Então('visualizo {string} na pagina pós cadastro') do |mensagem|
    expect(page).to have_content mensagem
    
end