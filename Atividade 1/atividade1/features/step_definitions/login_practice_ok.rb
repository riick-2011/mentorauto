Quando('preencho o campo de e-mail válido {string} e a senha {string}') do |email, paws|
login_page.login_page(email,paws)

end

Então('visualizo a pagina logada com {string}') do |account|
    screenshot
    expect(page).to have_content account
    find("#header > div.nav > div > div > nav > div:nth-child(2) > a").click 
end