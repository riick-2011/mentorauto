class LoginPage < SitePrism::Page
    set_url ' '
    element :btn_singin, '#header > div.nav > div > div > nav > div.header_user_info > a'
    element :btn_email, '#email'
    element :btn_paws, '#passwd'
    element :btn_login, '#SubmitLogin > span'

    def login_page(email, paws)
        btn_singin.click
        btn_email.set email
        btn_paws.set paws
        btn_login.click
    end

end