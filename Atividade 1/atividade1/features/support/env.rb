require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rspec'
require 'capybara/rspec/matchers'
require 'faker'
require 'report_builder'
require 'site_prism'
require 'json'
require 'pry'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require_relative 'page_helper.rb'

World Page
World Capybara::RSpecMatchers
World Capybara::DSL
BROWSER = ENV['BROWSER']

AMB = ENV["AMB"]

BASE_URL = YAML.load_file(File.dirname(__FILE__) + "/ambientes/ambientes.yml")[AMB]

if BROWSER == "firefox"
    @driver = :selenium

elsif BROWSER == "chrome"
    @driver = :selenium_chrome
    
elsif BROWSER == "chrome_headless"
    @driver = :selenium_chrome_headless
    
else ""
    raise "\n\n Por favor escolhe um browser \n\n"
end

ReportBuilder.configure do |config|
    config.json_path = "results/report.json" #pasta onde salva o json
    config.report_path = "results/report" #pasta onde salva o html
    config.report_types = [:html] #tipo de report a exportar
    config.report_title = "Report de testes" #nome do report
    config.color = "blue" #cor do report
    config.include_images = true #se coloca imagens ou não
    #config.additional_info = { browser: BROWSER, ambiente: AMB, user: "Agnaldo Vilariano"} 
end

    #essa variável ambiente recebe uma configuração do ENV. voce pode apagar os IFs ali e deixar o nome chumbado
    at_exit do
ReportBuilder.build_report
end

Capybara.configure do |config|
    config.default_driver = @driver
    config.app_host = BASE_URL["base_url"]
end