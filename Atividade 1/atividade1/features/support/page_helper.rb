# Pegando todos os arquivos que estao dentro do caminho a baixo
Dir[File.join(File.dirname(__FILE__), '../page/*.rb')].each { |file| require file }

module Page
    def login_page
        @login_page ||= LoginPage.new
    end
end