After do |scenario|
    scenario_name = scenario.name.gsub(/[^\w\-]/, '')
    if scenario.failed?
        print(scenario_name.downcase!, 'falhou')
    else
        print(scenario_name.downcase!, 'passou')
    end
end

def take_a_picture(file_name, resultado)
    data = Time.now.strftime('%F').to_s
    h_m_s = Time.now.strftime('%H%M%S%P').to_s
    temp_shot = page.save_screenshot("results/evidencias/#{data}/print_#{h_m_s}.png")
    
    attach(temp_shot, 'image/png')
end

def screenshot
    data = Time.now.strftime('%F').to_s
    h_m_s = Time.now.strftime('%H%M%S%P').to_s
    temp_shot = page.save_screenshot("results/evidencias/step/#{data}step/print_#{h_m_s}.print.png")
    
    attach(temp_shot, 'image/png')
end
