require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rspec'
require 'capybara/rspec/matchers'

World Capybara::RSpecMatchers
BROWSER = ENV['BROWSER']

AMB = ENV["AMB"]

BASE_URL = YAML.load_file(File.dirname(__FILE__) + "/ambientes/ambientes.yml")[AMB]

if BROWSER == "firefox"
    @driver = :selenium

elsif BROWSER == "chrome"
    @driver = :selenium_chrome
    
elsif BROWSER == "chrome_headless"
    @driver = :selenium_chrome_headless
    
else ""
    raise "\n\n Por favor escolhe um browser \n\n"
end

Capybara.configure do |config|
    config.default_driver = @driver
    config.app_host = BASE_URL["base_url"]
end