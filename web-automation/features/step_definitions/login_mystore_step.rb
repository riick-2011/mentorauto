Dado('que acesso ao site MyStore') do
    visit "/index.php"
    screenshot
  end
  
  Quando('acesso a pagina de Login com os dados {string} e {string}') do |login, passw|
    find("#_desktop_user_info > div > a > span").click
    find("#login-form > section > div:nth-child(2) > div.col-md-6 > input").set login
    find("#login-form > section > div:nth-child(3) > div.col-md-6 > div > input").set passw
    screenshot
    find("#submit-login").click
    
  end
  
  Então('visualizo {string} na pagina logada') do |mensagem|
    screenshot
    expect(page).to have_content mensagem 
     
  end